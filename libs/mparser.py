# Parser of the input files in pgen application.
# Copyright (C) 2018  Anonymous

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import sys
import shutil
from configparser import ConfigParser
from templateparser import TemplateParser


class Parser(object):
    def __init__(self, name, cpath, tpath):
        self.name = name
        self.cparser = ConfigParser(cpath)
        self.tparser = TemplateParser(tpath)
        self.delproject = False

    def create(self):
        try:
            self.cparser.parseStructure(self.name)
            self.tparser.replaceWord(self.cparser.getProjectId(),
                                     self.name)
            dic = self.cparser.getStructure()
            if os.path.isdir(self.name):
                raise RuntimeError("There is already a directory with name '" +
                                   self.name + "'")
            self.delproject = True
            for k, v in dic.items():
                if not os.path.exists(k):
                    os.makedirs(k)
                for ll in v:
                    self.createFile(k + "/" + ll[0],
                                    self.tparser.getContent(ll[1]))

        except Exception as ex:
            if self.delproject:
                shutil.rmtree(self.name)
            raise RuntimeError(ex)

    def createFile(self, name, content):
        with open(name, "w") as f:
            f.write(content if content is not None else "")
        f.close()
        self.changeMod(name)

    def changeMod(self, name):
        if sys.version_info[0] < 3:
            os.chmod(name, 0755)
        else:
            os.chmod(name, 0o755)
